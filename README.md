# Indexer

## Requisito

Certifique-se de ter o JDK 17 instalado no seu sistema.

## Como rodar?

1. Clone este repositório.
2. Coloque os arquivos de texto que deseja ler na pasta raiz deste projeto. Eles podem ser encontrados [nesse link](http://200.236.3.126:8999/ds143-texts/).
3. Abra o terminal no diretório do projeto.
4. Compile todas as classes Java com o seguinte comando:

   ``` javac -d "out" src/com/ds143/indexer/*.java ```
5. Execute o programa com o seguinte comando, substituindo [OPCAO], [PARAMETRO 1] e [PARAMETRO 2] pelos valores apropriados:

   ``` java -classpath ./out com.ds143.indexer.Main --[OPCAO] [PARAMETRO 1] [PARAMETRO 2] ```

## Sinopse

```
java -classpath ./out com.ds143.indexer.Main --freq N ARQUIVO
java -classpath ./out com.ds143.indexer.Main --freq-word PALAVRA ARQUIVO
java -classpath ./out com.ds143.indexer.Main --search TERMO ARQUIVO [ARQUIVO ...]
```

## Descrição

```
O programa **indexer** realiza uma contagem de palavras em documentos de 
texto. A partir dessa contagem, ele ainda permite uma busca pelo número de 
ocorrências de uma palavra específica em um documento, ou a seleção de 
documentos relevantes para um dado termo de busca.
O programa **indexer** transforma todas as letras para minúsculas e ignora
caracteres como números e pontuações.
Quando executado com a opção --freq, o programa **indexer** irá exibir o 
número de ocorrências das N palavras mais frequentes no documento passado 
como parâmetro, em ordem decrescente de ocorrências.
Quando executado com a opção --freq-word, o programa **indexer** exibe a 
contagem de uma palavra específica no documento passado como parâmetro.
Por fim, quando executado com a opção --search, o programa **indexer** 
apresenta uma listagem dos documentos mais relevantes para um dado termo de 
busca. Para tanto, o programa utiliza o cálculo TF-IDF (Term 
Frequency-Inverse Document Frequency).
```

## Opções

```
--freq N ARQUIVO
Exibe o número de ocorrência das N palavras que mais aparecem em ARQUIVO, em
ordem decrescente de ocorrência.
--freq-word PALAVRA ARQUIVO
Exibe o número de ocorrências de PALAVRA em ARQUIVO.
--search TERMO ARQUIVO [ARQUIVO ...]
Exibe uma listagem dos ARQUIVOS mais relevantes encontrados pela busca por
TERMO. A listagem é apresentada em ordem descrescente de relevância.
TERMO pode conter mais de uma palavra. Neste caso, deve ser indicado entre
àspas.
```


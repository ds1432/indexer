package com.ds143.indexer;

import java.io.IOException;
import java.util.Arrays;
import java.util.Locale;

public class Main {
    
    public static void main(String[] args) throws IOException {
    	long startTime = System.currentTimeMillis();
        String option = args[0];
        
        switch (option) {
            case "--freq":
            	String n = args[1];
            	String fileNameF = args[2];
                freq(n, fileNameF);
                break;
            case "--freq-word":
                String word = args[1].toLowerCase().trim();;
                String fileNameFw = args[2];
                freqWord(word, fileNameFw);
                break;
            case "--search":
            	String term = args[1];
                String[] fileNames = new String[args.length - 2];
                for (int i = 2; i < args.length; i++) {
                	fileNames[i - 2] = args[i];
                }
                search(term, fileNames);
                break;
        }
        
        long endTime = System.currentTimeMillis();
        long executionTime = endTime - startTime;
        double executionTimeSeconds = executionTime / 1000.0;
        String formattedExecutionTime = String.format("%.3f", executionTimeSeconds).replace(',', '.');
        System.out.println("\n# execution time : " + formattedExecutionTime + " seconds.");
    }

    private static void freq(String n, String fileName) throws IOException {
    	if (!stringIsPositiveNumbers(n)) {
    		System.out.println("\nAviso: eh aceito somente numeros positivos como entrada de dados!");
    	} else {
    		int number = Integer.parseInt(n);
    		WordHashMap wordHashMap = new WordHashMap();
        	wordHashMap.fillWordHashMap(fileName);
        	wordHashMap.getFreq(number);
    	}
    }

    private static void freqWord(String word, String fileName) throws IOException {
    	WordHashMap wordHashMap = new WordHashMap();
    	if (stringIsNumbersOrPunctuation(word) || word.isEmpty()) {
            System.out.println("\nAviso: eh aceito somente palavras como entrada de dados!");
        } else {
        	wordHashMap.fillWordHashMap(fileName);
            int wordCount = wordHashMap.getFreqWord(word);
            System.out.println("\nA palavra '" + word + "' aparece no arquivo '" + wordHashMap.getFileName() + "' " + wordCount + " vez(es)." );
        }
    }

    private static void search(String t, String[] fileNames) throws IOException {
    	String[] term = t.split("\\s+");
    	
    	if (arrayStringIsNumbersOrPunctuation(term)) {
            System.out.println("\nAviso: eh aceito somente palavras como entrada de dados!");
        } else {
        	int totalFiles = fileNames.length;
        	int totalFilesContainingTerm = 0;
        	WordHashMap[] wordHashMapList = new WordHashMap[totalFiles];
        	
        	int k = 0;
        	for (String word : term) {
        		String[] words = new String[word.toLowerCase().split("[^a-z]").length];
        		words = word.toLowerCase().split("[^a-z]");
        		for (String w : words) {
        	        if (w.length() >= 2) {
        	            k++;
        	        } else continue;

        	    }
        	} String[] termFixed = new String[k];
        	
        	int l = 0;
        	for (String word : term) {
        		String[] words = new String[word.toLowerCase().split("[^a-z]").length];
        		words = word.toLowerCase().split("[^a-z]");
        		for (String w : words) {
        	        if (w.length() >= 2) {
        	            termFixed[l] = w;
        	            l++;
        	        } else continue;
        	    }
        	} termFixed = Arrays.copyOf(termFixed, k);
        	
            for (int i = 0; i < wordHashMapList.length; i++) {
            	wordHashMapList[i] = new WordHashMap(termFixed.length);
            	wordHashMapList[i].fillWordHashMap(fileNames[i]);
            	wordHashMapList[i].getSearch(termFixed);
        		totalFilesContainingTerm += wordHashMapList[i].getContainsTerm();
            }
            
        	for (int i = 0; i < wordHashMapList.length; i++) {
        		
        		double[] termFrequencyList = new double[termFixed.length];
        		termFrequencyList = wordHashMapList[i].getTermFrequencyList();
        		double termFreqInverseDocumentFreqList = 0.0;
        		for (int j = 0; j < termFixed.length; j++) {
        			termFreqInverseDocumentFreqList += TermFreqInverseDocumentFreq.termFreqInverseDocumentFreqCalculator(
        					termFrequencyList[j],
            				totalFiles, 
            				totalFilesContainingTerm
            		);
        		} wordHashMapList[i].setTermFreqInverseDocumentFreqResult(termFreqInverseDocumentFreqList / termFixed.length);
        	}
        	
        	WordHashMap[] orderedWordHashMapList = new WordHashMap[totalFiles];
        	orderedWordHashMapList = ShellSortAlgorithm.shellSort(wordHashMapList);

        	System.out.println("\n-> Os arquivos em ordem de relevancia sao:");
            for (int j = 0; j < orderedWordHashMapList.length; j++) {
            	System.out.println(j + 1 + ") '" + wordHashMapList[j].getFileName() + "' que tem relevancia de " + String.format(Locale.US, "%.9f", orderedWordHashMapList[j].getTermFreqInverseDocumentFreqResult()) + " do calculo TF-IDF.");
            }
        }
    	
    }
    
    private static boolean stringIsPositiveNumbers(String string) {
        return string.matches("\\d+(\\.\\d+)?");
    }
    
    private static boolean stringIsNumbersOrPunctuation(String string) {
        return string.matches("[0-9\\p{Punct}]+");
    }
    
    private static boolean arrayStringIsNumbersOrPunctuation(String[] strings) {
    	int i = 0;
        for (String string : strings) {
            if (string.matches("[0-9\\p{Punct}]+")) {
            	i++;
            } else {
            	continue;
            }
        }
        if(strings.length == i) {
        	return true;
        } else {
        	return false;
        }
    }

}
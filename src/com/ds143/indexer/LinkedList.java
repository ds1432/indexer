package com.ds143.indexer;

public class LinkedList {
    Node head;

    public void insert(Entry data) {
        Node newNode = new Node();
        newNode.data = data;
        newNode.next = null;

        if (head == null) {
            head = newNode;
        } else {
            Node currentNode = head;
            while (currentNode.next != null) {
                currentNode = currentNode.next;
            }

            currentNode.next = newNode;
        }
    }

    public void show() {
        Node currentNode = head;
        
        do {
            System.out.println("[" + currentNode.data.getKey() + ", " + currentNode.data.getValue() + "]");
            currentNode = currentNode.next;
        } while (currentNode.next != null);
    }
}

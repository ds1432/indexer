package com.ds143.indexer;

public class TermFreqInverseDocumentFreq {
	
	public static double termFreqInverseDocumentFreqCalculator(double termFreqResult, int totalFiles, int totalFilesContainingTerm) {
        return termFreqResult * inverseDocumentFreqCalculator(totalFiles, totalFilesContainingTerm);
    }

    public static double termFreqCalculator(int totalTermFrequency, int totalWordsIncludingRepetitions) {
        return (double) totalTermFrequency / totalWordsIncludingRepetitions;
    }

    private static double inverseDocumentFreqCalculator(int totalFiles, int totalFilesContainingTerm) {
        return Math.log10((double) totalFiles / totalFilesContainingTerm);
    }
}

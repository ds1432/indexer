package com.ds143.indexer;

public class ShellSortAlgorithm {
	
	public static WordHashMap[] shellSort(WordHashMap[] arrayToSort) {
        int length = arrayToSort.length;
        int gap = length / 2;

        while (gap > 0) {
            for (int i = gap; i < length; i++) {
                WordHashMap temp = arrayToSort[i];
                int j = i;

                while (j >= gap && compareValues(arrayToSort[j - gap], temp) > 0) {
                	arrayToSort[j] = arrayToSort[j - gap];
                    j -= gap;
                }

                arrayToSort[j] = temp;
            }

            gap /= 2;
        }
        
        return arrayToSort;
    }

    private static int compareValues(WordHashMap a, WordHashMap b) {
        double valueA = a.getTermFreqInverseDocumentFreqResult();
        double valueB = b.getTermFreqInverseDocumentFreqResult();

        if (valueB < valueA) {
            return -1;
        } else if (valueB > valueA) {
            return 1;
        } else {
            return 0;
        }
    }

}

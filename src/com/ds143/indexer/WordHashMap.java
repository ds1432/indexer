package com.ds143.indexer;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;


public class WordHashMap {
	private LinkedList[] hashMap = new LinkedList[2];
    private String fileName = "";
    private int totalWords = 0;
    private int totalWordsIncludingRepetitions = 0;
    private int containsTerm = 0;
    private double[] termFrequencyList;
    private double termFreqInverseDocumentFreqResult = 0.0;
    
    public WordHashMap(){
    	
    }
    
    public WordHashMap(int termFixedLength){
    	double[] TermFrequencyList = new double[termFixedLength];
    	this.termFrequencyList = TermFrequencyList;
    }
    
    public void fillWordHashMap(String fileName) throws IOException {
    	this.fileName = fileName;
        Path filePath = Paths.get(fileName);
        try (Stream<String> lines = Files.lines(filePath, StandardCharsets.ISO_8859_1)) {
            lines.forEach(line -> {
                String[] words = line.toLowerCase().split("[^a-z]");
                for (String word : words) {
                    if (word.length() < 2) continue;
                    put(word, 1);
                }
            });
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void put(String key, int value) {
        if ((this.totalWords / this.hashMap.length) >= 8) resize();

        int listIndex = getIndex(key);

        if (this.hashMap[listIndex] == null) {
        	this.hashMap[listIndex] = new LinkedList();
        } else {
            Node currentNode = this.hashMap[listIndex].head;
            while (currentNode != null) {
                if (currentNode.data.getKey().equals(key)) {
                    currentNode.data.setValue(currentNode.data.getValue() + 1);
                    return;
                }
                currentNode = currentNode.next;
            }
        }

        this.hashMap[listIndex].insert(new Entry(key, value));
        this.totalWords++;
    }
    
    private int getIndex(String key) {
        return Math.abs(hashCode(key) % this.hashMap.length);
    }

    private int hashCode(String s) {
        int hash = 0;
        for (int i = 0; i < s.length(); i++) hash += (int) (s.charAt(i) * Math.pow(31, i));
        return hash;
    }
    
    private void resize() {
        LinkedList[] oldHashMap = this.hashMap;
        this.hashMap = new LinkedList[oldHashMap.length * 2];
        this.totalWords = 0;

        for (int i = 0; i < oldHashMap.length; i++) {
            if (oldHashMap[i] == null) continue;
            
            Node currentNode = oldHashMap[i].head;
            
            while (currentNode != null) {
                put(currentNode.data.getKey(), currentNode.data.getValue());
                currentNode = currentNode.next;
            }
        }
    }
    
    public void getFreq(int n) {
        Entry[] unorderedWordList = new Entry[totalWords];
        Entry[] orderedWordList = new Entry[totalWords];
        
        int i = 0;
        for (LinkedList listIndex : this.hashMap) {
            if (listIndex != null) {
                Node currentNode = listIndex.head;
                
                while (currentNode != null) {
                	unorderedWordList[i] = new Entry(currentNode.data.getKey(), currentNode.data.getValue());
                    currentNode = currentNode.next;
                    i++;
                }
            }
        }
        
        orderedWordList = QuickSortAlgorithm.quickSort(unorderedWordList);
        //orderedWordList = MergeSortAlgorithm.mergeSort(unorderedWordList);
        
        System.out.println("\n-> As " + n + " palavras mais frequentes no arquivo '" + fileName + "' sao:");
        for (int j = 0; j < n; j++) {
        	if (j > totalWords - 1) {
        		System.out.println("\nAviso: o documento nao possui mais palavras para serem exibidas!");
        		break;
        	}
        	System.out.println(j + 1 + ") '" + orderedWordList[j].getKey() + "' que se repetiu " + orderedWordList[j].getValue() + " vez(es).");
        }
        
    }

    public int getFreqWord(String key) {
        int listIndex = getIndex(key);
        if (this.hashMap[listIndex] == null) return 0;
        Node currentNode = this.hashMap[listIndex].head;
        
        while (currentNode != null) {
            if (currentNode.data.getKey().equals(key)) return currentNode.data.getValue();
            currentNode = currentNode.next;
        }

        return 0;
    }
    
    public void getSearch(String[] termFixed) {
    	
        for (LinkedList listIndex : this.hashMap) {
            if (listIndex != null) {
                Node currentNode = listIndex.head;
                
                while (currentNode != null) {
                	this.totalWordsIncludingRepetitions += currentNode.data.getValue();
                    currentNode = currentNode.next;
                }
            }
        }

        int[] termFrequencyList = new int[termFixed.length];
    	boolean containsTermIsFalse = false;
    	int k = 0;
    	for (String word : termFixed) {
            int freqWord = getFreqWord(word);
            termFrequencyList[k] = freqWord;
            
            if (freqWord == 0) containsTermIsFalse = true;
            k++;
        }
    	
    	for (int i = 0; i < termFrequencyList.length; i++) {
    		this.termFrequencyList[i] = TermFreqInverseDocumentFreq.termFreqCalculator(termFrequencyList[i], this.totalWordsIncludingRepetitions);
    	}
    	
    	int hasTermFrequency = 0;
    	for (int j = 0; j < termFrequencyList.length; j++) {
    	    hasTermFrequency += termFrequencyList[j];
    	}
    	
    	if (!containsTermIsFalse && hasTermFrequency > 0) {
    		this.containsTerm = 1;
    	} else {
    		this.containsTerm = 0;
    	}
    	
    }
    
	public String getFileName() {
		return fileName;
	}
    
    public int getTotalWordsIncludingRepetitions() {
		return totalWordsIncludingRepetitions;
	}

	public int getContainsTerm() {
        return this.containsTerm;
    }
	
	public double getTermFreqInverseDocumentFreqResult() {
		return termFreqInverseDocumentFreqResult;
	}
	
	public double[] getTermFrequencyList() {
		return termFrequencyList;
	}
	
	public void setTermFreqInverseDocumentFreqResult(double termFreqInverseDocumentFreqResult) {
		this.termFreqInverseDocumentFreqResult = termFreqInverseDocumentFreqResult;
	}
	
}

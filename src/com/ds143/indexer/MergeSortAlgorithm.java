package com.ds143.indexer;

public class MergeSortAlgorithm {

    public static Entry[] mergeSort(Entry[] arrayToSort) {
        if (arrayToSort.length <= 1) {
            return arrayToSort;
        }

        int middle = arrayToSort.length / 2;
        Entry[] leftHalf = copyOfRange(arrayToSort, 0, middle);
        Entry[] rightHalf = copyOfRange(arrayToSort, middle, arrayToSort.length);

        leftHalf = mergeSort(leftHalf);
        rightHalf = mergeSort(rightHalf);

        return merge(leftHalf, rightHalf);
    }

    private static Entry[] merge(Entry[] left, Entry[] right) {
        int leftSize = left.length;
        int rightSize = right.length;
        int totalSize = leftSize + rightSize;

        Entry[] result = new Entry[totalSize];
        int leftIndex = 0, rightIndex = 0, resultIndex = 0;

        while (leftIndex < leftSize && rightIndex < rightSize) {
            if (left[leftIndex].getValue() >= right[rightIndex].getValue()) {
                result[resultIndex++] = left[leftIndex++];
            } else {
                result[resultIndex++] = right[rightIndex++];
            }
        }

        while (leftIndex < leftSize) {
            result[resultIndex++] = left[leftIndex++];
        }

        while (rightIndex < rightSize) {
            result[resultIndex++] = right[rightIndex++];
        }

        return result;
    }
    
    public static Entry[] copyOfRange(Entry[] original, int from, int to) {
        int newLength = to - from;
        if (newLength < 0) {
            throw new IllegalArgumentException("Invalid range: from (" + from + ") > to (" + to + ")");
        }

        Entry[] copy = new Entry[newLength];
        for (int i = 0; i < newLength; i++) {
            copy[i] = original[from + i];
        }

        return copy;
    }
    
}


package com.ds143.indexer;

import java.util.Comparator;
import java.util.Random;

public class QuickSortAlgorithm {
	
	public static Entry[] quickSort(Entry[] arrayToSort) {
        shuffle(arrayToSort);
        quickSortRecursive(arrayToSort, 0, arrayToSort.length - 1);
        return arrayToSort;
    }
    
	private static void shuffle(Entry[] arrayToSort) {
        Random rand = new Random();
        int length = arrayToSort.length;
        if (length > 1) {
            for (int i = 0; i < length - 1; i++) {
                int j = i + rand.nextInt(length - i);
                Entry temporary = arrayToSort[j];
                arrayToSort[j] = arrayToSort[i];
                arrayToSort[i] = temporary;
            }
        }
    }
	
	private static void quickSortRecursive(Entry[] arrayToSort, int low, int high) {
        if (high <= low) return;
        int partitionIndex = partition(arrayToSort, low, high);
        quickSortRecursive(arrayToSort, low, partitionIndex - 1);
        quickSortRecursive(arrayToSort, partitionIndex + 1, high);
    }
	
	private static int partition(Entry[] arrayToSort, int low, int high) {
        Comparator<Entry> comparator = Comparator.comparingInt(Entry::getValue).reversed();
        int i = low, j = high + 1;
        while (true) {
            while (comparator.compare(arrayToSort[++i], arrayToSort[low]) < 0)
                if (i == high) break;

            while (comparator.compare(arrayToSort[low], arrayToSort[--j]) < 0)
                if (j == low) break;

            if (i >= j) break;
            swap(arrayToSort, i, j);
        }

        swap(arrayToSort, low, j);
        return j;
    }

	private static void swap(Entry[] arrayToSort, int i, int j) {
        Entry temporary = arrayToSort[i];
        arrayToSort[i] = arrayToSort[j];
        arrayToSort[j] = temporary;
    }

}
